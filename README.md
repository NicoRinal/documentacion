Comandos para correr la pagina web:
	python manage.py runserver
	python manage.py mqtt

Comandos para cargar los archivos al ESP8266:
    ampy --port /serial/port --baud 115200 put 'configESP.json'
	ampy --port /serial/port --baud 115200 put 'umqttsimple.py'
	ampy --port /serial/port --baud 115200 put 'robust.py'
	ampy --port /serial/port --baud 115200 put 'main.py'
	ampy --port /serial/port --baud 115200 put 'boot.py'
Si desea ver los datos que tira el ESP8266 debera correr el comando: minicom	
